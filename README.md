# Capture the flag for CS:GO


Capture the flag is pretty old gamemode, where players need to capture the enemy's flag and bring it back to their own one.
Models + some ideas are taken from here https://forums.alliedmods.net/showthread.php?t=171719 . As it was not working on CS:GO, I decided to convert this for everyone!


# Installation

* Download all the files
* Extract all the files inside the right paths.
* Go inside some map and write !ctf , setup flag spawn positions for each team
* Restart the map


# Usefull plugin to CTF:

* MODELS IN MAP: https://forums.alliedmods.net/showthread.php?p=2389415

Platform model: https://www.dropbox.com/s/5ovlgnpf70jy1du/Platform%20model.zip?dl=1

* SPAWN PROTECTION: https://forums.alliedmods.net/showthread.php?p=2324123


# Commands

| Command       | Meaning       | Flag  |
| ------------- |:-------------:| -----:|
| !ctf          | Allows to set spawn position for flags | b     |
